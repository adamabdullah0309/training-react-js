import React, {Component} from 'react'
import classes from './Person.module.css';

import { render } from 'react-dom';


class Person extends Component {
    render()
    {
        console.log('person.js rendering...')
        return ( 
            <div className={classes.Person}>
                <p onClick={this.props.click}>halo nama saya adalah {this.props.name} umur saya = {this.props.age}</p> 
                {/* <p>{props.children}</p> */}
                <input type="text" onChange={this.props.changed} value={this.props.name} ></input>
            </div>
        );
    }
    
}

export default Person;