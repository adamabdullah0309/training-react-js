import React, {Component} from 'react'
import Person from './Person/Person'

class Persons extends Component
{
    shouldComponentUpdate(nextProps, nextState){
        console.log('[persons.js] shouldComponentUpdate');
        if(nextProps.persons !== this.props.person ||
            nextProps.changed !== this.props.changed ||
            nextProps.clicked !== this.props.clicked
            )
        {
            return true;
        }
        else{
            return false;
        }
    }

    componentWillUnmount(){
        console.log('[persons.js] componentwillunmount')
    }

    render()
    {
        console.log('persons.js rendering...')
        return (
            this.props.persons.map( (person, index) => {
                return (
                    <Person 
                    name={person.name} 
                    age={person.age}
                    key={person.id} 
                    click={() => this.props.clicked( index )}
                    changed={(event) =>this.props.changed(event, person.id)}
                    />
                );
            })
        );
    }
    
}
export default Persons;