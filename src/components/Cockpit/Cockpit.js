import React, { useEffect } from 'react'

import classes from './Cockpit.module.css'

const Cockpit = (props) =>{

    useEffect(() =>{
        console.log('ini dirender ulang terus karena di app dipanggilterus')
        //ini bisa dibuat untuk memanggil http request
        const timer = setTimeout(()=>{
            alert('Saved to cloud!');
        }, 1000);
        return () =>{
            clearTimeout(timer)
            console.log('[Cockpit.js] cleanup work in useEffect ')
        }
    }, [] //ini kegunaan untuk menentukan berjalannya useEffect. klo array kosong diproses hanya sekali. klo diisi data. maka akan terpanggil jika data tersebut berubah
    );

    useEffect(() =>{
        console.log('[Cockpit.js] ini dirender ulang terus karena di app  dipanggilterus 2nd')
        //ini bisa dibuat untuk memanggil http request
        
        return () =>{
            console.log('[Cockpit.js] 2nd cleanup work in 2nd useEffect ')
        }
    }, //ini kegunaan untuk menentukan berjalannya useEffect. klo array kosong diproses hanya sekali. klo diisi data. maka akan terpanggil jika data tersebut berubah
    );


    const AssignedClasses = [];
    let btnClass = '';
    if(props.showPersons)
    {
        btnClass = classes.Red
    }

    if( props.persons <= 2 ){
        AssignedClasses.push(classes.red);
    }

    if( props.persons <= 1 ){
        AssignedClasses.push(classes.bold);
    }
    
    return (
        <div className={classes.Cockpit}>
            <h1>{props.title}</h1>
            <p className={AssignedClasses.join(' ')}>This is really working</p>
            <button 
                onClick={props.clicked} 
                className={btnClass}
                >Toggle Person
            </button>
           {' '}
            <button 
                >Add new Person
            </button>
        </div>
    );
};

export default React.memo(Cockpit);