import React, { Component, useState } from 'react';

import Person from '../components/Persons/Person/Person'
import Persons from '../components/Persons/Persons'
import classes from './App.module.css';
import Cockpit from '../components/Cockpit/Cockpit'
import Coba from '../components/Cockpit/Coba'
import { render } from 'react-dom';
import { DatePicker, message,Button } from 'antd';
import styles from './mystyle.module.css';

import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

// import SockJS from 'https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js';

// import 'antd/lib/button/style';
// import coba from 'antd/lib/spin/style/css';

import "antd/dist/antd.css";



var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];
var stompClient = null;
var username = null;
const usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');
class Socket extends Component {
    constructor(props)
    {
        super(props);
        console.log('[App.js] constructor')
    }

    state = {
        persons : [
            { id:'asd', name : 'Max', age:28 },
            { id:'asdsd', name : 'Manu', age:29 },
            { id:'asdffs', name : 'Stephanie', age:26 }

        ],
        angka : 1,
        showPerson : false,
        showCockpit : true
    }

    static getDerivedStateFromProps(props, state)
    {

        console.log('[App.js] getDerivedStateFromProps', props);
        return state;
    }

    namechangeHandler = ( event,id  ) =>{
        const personIndex = this.state.persons.findIndex(p =>{
            return p.id === id;
        });

        const person = {
            ...this.state.persons[personIndex]
        }

        person.name = event.target.value;

        const persons = [...this.state.persons];
        persons[personIndex] = person;


        this.setState({persons : persons});
    }

    deletePerson = (PersonIndex) =>
    {
        // const persons1 = this.state.persons.slice();
        const persons1 = [...this.state.persons]
        persons1.splice(PersonIndex,1);
        this.setState({persons:persons1});
    }

    handlerToggle = () =>
    {
        const doesShow = this.state.showPerson;
        this.setState({showPerson : !doesShow});
    }
    
    cobaHandler = (tambah) =>
    {
        let angka1 = this.state.angka + 1;
        this.setState({angka : angka1});
        console.log('halo', this.state.angka);
    }

    componentDidMount()
    {
        console.log('App.js componentDidMount')
    }

    
dcccc = () =>
{
    console.log('sdsd')
}

connect = () => {
    let username = document.querySelector('#name').value.trim();
    console.log('conek cu')
    if(username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        let stompClient = Stomp.over(socket);

        stompClient.connect({}, this.onConnected(), this.onError());
    }
    // event.preventDefault();
}

onError = () => {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}



getAvatarColor = (messageSender) => {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }
    var index = Math.abs(hash % colors.length);
    return colors[index];
}

onMessageReceived = (payload) => {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' joined!';
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = this.getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}

onConnected = () => {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/public', this.onMessageReceived());

    // Tell your username to the server
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    )

    connectingElement.classList.add('hidden');
}

    render (){
        console.log('haloo')
        // console.log('[app.js] render')
        // let persons = null;
        // if ( this.state.showPerson ) {
        //     persons = (
        //             <Persons 
        //                 persons={this.state.persons}
        //                 clicked = {this.deletePerson}
        //                 changed = {this.namechangeHandler}
        //             />
        //     )
        // }

//         'use strict';









function sendMessage(event) {
    var messageContent = messageInput.value.trim();
    if(messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            content: messageInput.value,
            type: 'CHAT'
        };
        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}





// usernameForm.addEventListener('submit', connect, true)
// messageForm.addEventListener('submit', sendMessage, true)

        return (
    <div>    
    <div id="username-page">
    <div class="username-page-container">
        <h1 class="title">Type your username</h1>
        <form id="usernameForm" name="usernameForm">
            <div class="form-group">
                <input type="text" id="name" placeholder="Username" autocomplete="off" class="form-control" />
            </div>
            <div class="form-group">
                <button onClick={() => {this.connect();}} class="accent username-submit">Start Chatting</button>
            </div>
        </form>
    </div>
</div>

<div id="chat-page" class="hidden">
    <div class="chat-container">
        <div class="chat-header">
            <h2>Spring WebSocket Chat Demo</h2>
        </div>
        <div class="connecting">
            Connecting...
        </div>
        <ul id="messageArea">

        </ul>
        <form id="messageForm" name="messageForm">
            <div class="form-group">
                <div class="input-group clearfix">
                    <input type="text" id="message" placeholder="Type a message..." autocomplete="off" class="form-control"/>
                    <button type="submit" class="primary">Send</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
        );
    }
}

export default Socket;
