import React, { Component, useState } from 'react';

import Person from '../components/Persons/Person/Person'
import Persons from '../components/Persons/Persons'
import classes from './App.module.css';
import Cockpit from '../components/Cockpit/Cockpit'
import Coba from '../components/Cockpit/Coba'
import { render } from 'react-dom';
import { DatePicker, message,Button } from 'antd';
import styles from './mystyle.module.css'; 
// import 'antd/lib/button/style';
// import coba from 'antd/lib/spin/style/css';

import "antd/dist/antd.css";
class App2 extends Component {
    constructor(props)
    {
        super(props);
        console.log('[App.js] constructor')
    }

    state = {
        persons : [
            { id:'asd', name : 'Max', age:28 },
            { id:'asdsd', name : 'Manu', age:29 },
            { id:'asdffs', name : 'Stephanie', age:26 }

        ],
        angka : 1,
        showPerson : false,
        showCockpit : true
    }

    static getDerivedStateFromProps(props, state)
    {

        console.log('[App.js] getDerivedStateFromProps', props);
        return state;
    }

    //update
    namechangeHandler = ( event,id  ) =>{
        const personIndex = this.state.persons.findIndex(p =>{
            return p.id === id;
        });

        const person = {
            ...this.state.persons[personIndex]
        }

        person.name = event.target.value;

        const persons = [...this.state.persons];
        persons[personIndex] = person;


        this.setState({persons : persons});
    }

    deletePerson = (PersonIndex) =>
    {
        // const persons1 = this.state.persons.slice();
        const persons1 = [...this.state.persons]
        persons1.splice(PersonIndex,1);
        this.setState({persons:persons1});
    }

    handlerToggle = () =>
    {
        const doesShow = this.state.showPerson;
        this.setState({showPerson : !doesShow});
    }
    
    cobaHandler = (tambah) =>
    {
        let angka1 = this.state.angka + 1;
        this.setState({angka : angka1});
        console.log('halo', this.state.angka);
    }

    componentDidMount()
    {
        console.log('App.js componentDidMount')
    }

    render (){
        console.log('[app.js] render')
        let persons = null;
        if ( this.state.showPerson ) {
            persons = (
                    <Persons 
                        persons={this.state.persons}
                        clicked = {this.deletePerson}
                        changed = {this.namechangeHandler}
                    />
            )
        }

         

        return (
            // <div className={styles.App}>Hello Car!</div>
            <div className={classes.App}>
                
                <button onClick={() =>{this.setState({ showCockpit:false })}} >Remove Cockpit</button>
                {this.state.showCockpit ? (<Cockpit 
                    title={this.props.appTitle}
                    showPersons={this.state.showPerson}
                    persons={this.state.persons.length}
                    clicked = {this.handlerToggle}
                    />) : null}
                {persons} 
                
            </div>
        );
    }
}

export default App2;
